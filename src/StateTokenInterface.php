<?php

namespace Drupal\openid_connect_rest;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a state token entity type.
 *
 * @package Drupal\openid_connect_rest
 *
 * @ingroup openid_connect_rest
 */
interface StateTokenInterface extends ConfigEntityInterface {

}
