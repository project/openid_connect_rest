<?php

namespace Drupal\openid_connect_rest;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an authorization mapping entity type.
 *
 * @package Drupal\openid_connect_rest
 *
 * @ingroup openid_connect_rest
 */
interface AuthorizationMappingInterface extends ConfigEntityInterface {

}
